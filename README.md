To run:
1. source .venv/train/bin/activate
2. sudo python app.py
2. Create and push branch (create_mr.sh cane be used to create branch)
3. Create merge request
4. Set label "Ready for merge" on merge request
5. Wait untill pipeline finishes
6. Check that changes from branch are merged to master